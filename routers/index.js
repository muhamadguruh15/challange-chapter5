const express = require("express");
const router = express.Router();

app.use(express.static(path.join(__dirname, "public")));

// CHAPTER 3 - HOMEPAGE
router.get("/", (req, res) => {
    res.render('index.ejs')
})

// CHAPTER 4 - GAME
router.get("/game", (req, res) => {
    res.render('mini-game.ejs')
})