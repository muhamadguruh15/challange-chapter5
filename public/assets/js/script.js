const button = document.querySelectorAll(".button")
const result = document.getElementById("result");
const comRock = document.getElementById("com-rock");
const comScissors = document.getElementById("com-scissors");
const comPaper = document.getElementById("com-paper");
const refresh = document.getElementById('refresh');
const boxInfo = document.getElementById('boxInfo');
const textInfo = document.getElementById('textInfo');
const addElement1 = document.querySelectorAll(".stop");

let tempResult;

class Player {
    constructor(name) {
        this.name = name;
    };
    clicking(value) {
        console.log(value);
    }
    random() {
        let choice = ['Rock', 'Scissors', 'Paper' ];
        let randomChoice = Math.floor(Math.random() * choice.length);
        return choice[randomChoice];
    }
}

class MainPlayer extends Player {
    constructor(name) {
        super(name);
    }
}

class Robot extends Player {
    constructor(name) {
        super(name);
    }

    random() {
        super.random;
        let choice = ['Rock', 'Scissors', 'Paper' ];
        let randomChoice = Math.floor(Math.random() * choice.length);
        return choice[randomChoice];
    }
}

let player1 = new MainPlayer("Guruh");
let computer = new Robot("Smart Computer");

button.forEach(element => {
    element.onclick = (v) => {
        tempResult = v.currentTarget.value;
        let enemyChoosen = computer.random();
        element.classList.add('chosen');
        console.log(tempResult);
        console.log(enemyChoosen);
        getWinner(tempResult, enemyChoosen);
    }
});

// Function Box
// Win BOX
function resultWin() {
    boxInfo.classList.add('winBox');
    textInfo.setAttribute("style", "font-size:36px; color:white");
}

// Draw BOX
function resultDraw() {
    boxInfo.classList.add('drawBox');
    textInfo.setAttribute("style", "font-size:36px; color:white");
}

// Win BOX
function resultLose() {
    boxInfo.classList.add('loseBox');
    textInfo.setAttribute("style", "font-size:36px; color:white");
}

// Function Result
// Win
function win() {
    console.log("Player Win");
    addElement1.forEach(addElement3 => {
        addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;")
    })
    resultWin();
    textInfo.innerText = "Player Win";
}

// Draw
function draw() {
    console.log("Draw");
    addElement1.forEach(addElement3 => {
        addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;")
    })
    resultDraw();
    textInfo.innerText = "Draw";
}

// Lose
function lose() {
    console.log("Computer Win");
    addElement1.forEach(addElement3 => {
        addElement3.setAttribute("style", "cursor: not-allowed;pointer-events: none;")
    })
    resultLose();
    textInfo.innerText = "Computer Win" ;
}


function getWinner(firstPlayer, secondPlayer) {

    switch (firstPlayer + secondPlayer) {
        case "RockScissors":
        case "ScissorsPaper":
        case "PaperRock":
            return win();
        
            break;
        case "RockRock":
        case "ScissorsScissors":
        case "PaperScissors":
            return draw();

            break;
        case "ScissorsRock":
        case "PaperScissors":
        case "RockPaper":
            return lose();
        
            break;
    }

}

refresh.addEventListener('click', function () {

    addElement1.forEach(addElement2 => {
        addElement2.classList.remove('chosen');
    })

    addElement1.forEach(addElement3 => {
        addElement3.removeAttribute("style", "cursor: not-allowed; pointer: none;")
    })

    // remove Box Info
    boxInfo.classList.remove('winBox');
    boxInfo.classList.remove('drawBox');
    boxInfo.classList.remove('loseBox');
    
    // remove text attribute
    textInfo.removeAttribute("style", "color: '', font-size: '' ");

    textInfo.style.marginTop = null;
    textInfo.style.fontSize = null;
    textInfo.innerText = "VS";
    // button.disabled = false;
})

