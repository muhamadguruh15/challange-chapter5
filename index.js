// IMPORT THIRD PARTY MODULE
const express = require("express");
const path = require("path")

// BUAT OBJECT BARU DARI EXPRESS
const app = express();

// MENENTUKAN PORT YANG AKAN DIGUNAKAN
const port = 8080;

// MIDDLEWARE BUATAN SENDIRI
const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`)
    next()
}
app.use(express.static(path.join(__dirname, "public")));

app.set("view engine", "ejs");

// MEMASANG MIDDLEWARE UNTUK PARSING DATA YG 
// REQUESTNYA Content-Type nya : application/json
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

// TAMBAH MIDDLEWARE BUATAN SENDIRI
app.use(logger);

const userManagement = require("./controllers/Users");

app.use(userManagement);

// CHAPTER 3 - GAME
app.get("/", (req, res) => {
    res.render('index.ejs')
})

// CHAPTER 4 - GAME
app.get("/game", (req, res) => {
    res.render('mini-game.ejs')
})

// INTERNAL SERVER ERROR HANDLER
app.use((err, req, res, next) => {
    console.log(err)
    res.status(500).json({
        status: "fail",
        errors: err.message
    })
})

// DAFTARIN SEMUA ENDPOINT YANG DITETAPIN KE app
app.listen(port, () => console.log(`Server udah jalan cuy! Di port ${port}`));